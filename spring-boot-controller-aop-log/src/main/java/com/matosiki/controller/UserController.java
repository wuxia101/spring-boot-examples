package com.matosiki.controller;

import com.matosiki.annotation.OpLogger;
import com.matosiki.annotation.OpType;
import com.matosiki.domain.User;
import com.matosiki.pojo.ExecutionResult;
import com.matosiki.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class);


    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("executionResult")
    private ExecutionResult result;

    @RequestMapping(value = "/userlist")
    @OpLogger(description = "查询用户信息", invocationType = OpType.QUERY)
    @ResponseBody
    public ExecutionResult getUserList(String id)  {
            List<User> users = userService.findAll();
            result.setCode(ExecutionResult.ReturnCode.RES_SUCCESS);
            result.setData(users);
            result.setMessage("查询成功！");
            //异常处理
            int aa = (int) 5 / 0;

        return result;
    }
}

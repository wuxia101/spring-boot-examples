package com.matosiki.annotation;

public enum OpType {
    ADD("添加"), UPDATE("修改更新"), DELETE("删除"), QUERY("查询"), ADD_UPDATE("添加或更新");

    OpType(String opType) {
        this.opType = opType;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    private String opType;
}

package com.matosiki.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
@Documented
public @interface OpLogger {

    public abstract String description() default "";

    public abstract OpType invocationType() default OpType.QUERY;
}

package com.matosiki.service;


import com.matosiki.domain.OpLog;

import java.util.List;

/**
 * @Description:
 * @Author: vesus
 * @CreateDate: 2018/5/20 上午11:43
 * @Version: 1.0
 */
public interface OpLogService {

    public List<OpLog> findAll();

    public void saveLog(OpLog log);

    public OpLog findOne(long id);

    public void delete(long id);

}

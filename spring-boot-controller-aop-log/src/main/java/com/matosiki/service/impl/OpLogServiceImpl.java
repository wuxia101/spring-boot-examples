package com.matosiki.service.impl;

import com.matosiki.domain.OpLog;
import com.matosiki.repository.OpLogRepository;
import com.matosiki.service.OpLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: vesus
 * @CreateDate: 2018/5/20 上午11:45
 * @Version: 1.0
 */
@Service
public class OpLogServiceImpl implements OpLogService {

    @Autowired
    OpLogRepository opLogRepository;
    @Override
    public List<OpLog> findAll() {
        return null;
    }

    @Override
    public void saveLog(OpLog log) {
        opLogRepository.save(log);
    }

    @Override
    public OpLog findOne(long id) {
        return null;
    }

    @Override
    public void delete(long id) {

    }
}

package com.matosiki.repository;

import com.matosiki.domain.OpLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Author: vesus
 * @CreateDate: 2018/5/20 上午11:31
 * @Version: 1.0
 */
@Repository("operationLogRepository")
public interface OpLogRepository extends CrudRepository<OpLog,Long> {
}

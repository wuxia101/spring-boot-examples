package com.matosiki.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.matosiki.pojo.ExecutionResult;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:/js/");
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }


    /**
     * 注入结果返回公用实体封装
     *
     * @return
     */
    @Bean
    @Scope(scopeName = "prototype")
    public ExecutionResult executionResult() {
        return new ExecutionResult("获取成功", ExecutionResult.ReturnCode.RES_SUCCESS);
    }


    @Bean("objectMapper")
    @Scope(scopeName = "prototype")
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }


}
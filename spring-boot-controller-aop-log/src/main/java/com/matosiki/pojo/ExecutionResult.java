package com.matosiki.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

public class ExecutionResult  {

    private String message;
    private ReturnCode code;
    private Object data;

    public ExecutionResult() {
    }

    public ExecutionResult(String message, ReturnCode code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReturnCode getCode() {
        return code;
    }

    public void setCode(ReturnCode code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public enum ReturnCode {
        RES_SUCCESS("0"), RES_FAILED("-1");
        public String code;

        ReturnCode(String code) {
            this.code = code;
        }

        @JsonValue
        public String getCode() {
            return code;
        }

    }

}

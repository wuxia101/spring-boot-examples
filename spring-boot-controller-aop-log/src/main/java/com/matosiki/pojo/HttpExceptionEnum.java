package com.matosiki.pojo;

public enum  HttpExceptionEnum {
    NOT_SUPPORTED_METHOD_EXCEPTION, MISSING_REQUEST_PART_EXCEPTION, NOT_SUPPORTED_MEDIA_TYPE_EXCEPTION, NOT_ACCEPTABLE_MEDIA_TYPE_EXCEPTION, MISSING_REQUEST_PARAMETER_EXCEPTION, REQUEST_BINDING_EXCEPTION, NOT_SUPPORTED_CONVERSION_EXCEPTION, TYPE_MISMATCH_EXCEPTION, MESSAGE_NOT_READABLE_EXCEPTION, MESSAGE_NOT_WRITABLE_EXCEPTION, NOT_VALID_METHOD_ARGUMENT_EXCEPTION, BIND_EXCEPTION, ASYNC_REQUEST_TIMEOUT_EXCEPTION, UNKNOWN_EXCEPTION, NOT_FOUND_EXCEPTION;

    private Object code;
    private String message;

    public Object getCode() {

        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

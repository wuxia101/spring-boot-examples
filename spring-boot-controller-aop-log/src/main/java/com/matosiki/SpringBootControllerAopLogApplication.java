package com.matosiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootControllerAopLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootControllerAopLogApplication.class, args);
    }
}

package com.matosiki.exception;

import com.matosiki.pojo.ExecutionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述:
 * 全局异常处理类  适合多种复杂自定义异常并结合 @Validated 一起使用
 *
 * @author: wuxia
 * @date: 11:13 2018/10/15
 */
//@ControllerAdvice  todo 使用时请开启
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    private ExecutionResult result;

    /**
     * 处理所有不可知的异常
     *
     * @param e
     * @return
     */
    ExecutionResult handleException(Exception e) {
        logger.error(e.getMessage(), e);
        result.setMessage(e.getMessage());
        result.setCode(ExecutionResult.ReturnCode.RES_FAILED);
        return result;
    }

    /**
     * 功能描述:
     * 这种异常处理类适合自定义方式定义
     * 然后去捕获 处理
     *
     * @author: wuxia
     * @date: 11:22 2018/10/15
     * @param:
     * @return:
     */

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExecutionResult> handleException(Exception exception, HttpServletRequest request) {
        logger.error("系统异常!", exception);
        result.setMessage("操作失败，请联系管理员！");
        result.setCode(ExecutionResult.ReturnCode.RES_FAILED);
        return new ResponseEntity<ExecutionResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

package com.matosiki.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.matosiki.pojo.ExecutionResult;
import com.matosiki.pojo.HttpExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * 功能描述:
 *  统一异常处理解析类
 *  另一种解决方案： @ControllerAdvice + @ ExceptionHandler
 *  @see  GlobalExceptionHandler  如果两个都使用的话 该处理器会后调用
 *
 * @author: wuxia
 * @date: 11:11 2018/10/15
 * @param:
 * @return:
 *
 */
@Component
public class CommonException implements HandlerExceptionResolver {
    private static Logger logger = LoggerFactory.getLogger(CommonException.class);

    @Autowired
    @Qualifier("executionResult")
    private ExecutionResult result;

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        logger.info("进入异常处理公共方法！");
        result.setCode(ExecutionResult.ReturnCode.RES_FAILED);
        result.setMessage(ex.getMessage());
        result.setData(null);
        return jsonResult(result);
    }

    /**
     * 返回错误页
     *
     * @param message
     *         错误信息
     * @param url
     *         错误页url
     * @return 模型视图对象
     */
    private ModelAndView pageResult(String message, String url) {
        Map<String, String> model = new HashMap<String, String>();
        model.put("errorMessage", message);
        return new ModelAndView(url, model);
    }


    /**
     * 返回错误数据
     *
     * @param result
     *         错误信息
     * @return 模型视图对象
     */
    private ModelAndView jsonResult(ExecutionResult result) {
        ModelAndView mv = new ModelAndView();
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2JsonView view = new MappingJackson2JsonView(mapper);
        view.setAttributesMap(mapper.convertValue(result, Map.class));
        mv.setView(view);
        return mv;
    }


    /**
     * 这个方法是拷贝 {@link org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver#doResolveException},
     * 加入自定义处理，实现对400， 404， 405， 406， 415， 500(参数问题导致)， 503的处理
     *
     * @param ex
     *         异常信息
     * @param request
     *         当前请求对象(用于判断当前请求是否为ajax请求)
     * @return 视图模型对象
     */
    private ModelAndView specialExceptionResolve(Exception ex, HttpServletRequest request) {
        try {
            if (ex instanceof NoHandlerFoundException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_FOUND_EXCEPTION, request);
            } else if (ex instanceof HttpRequestMethodNotSupportedException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_SUPPORTED_METHOD_EXCEPTION, request);
            } else if (ex instanceof HttpMediaTypeNotSupportedException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_SUPPORTED_MEDIA_TYPE_EXCEPTION, request);
            } else if (ex instanceof HttpMediaTypeNotAcceptableException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_ACCEPTABLE_MEDIA_TYPE_EXCEPTION, request);
            } else if (ex instanceof MissingPathVariableException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_SUPPORTED_METHOD_EXCEPTION, request);
            } else if (ex instanceof MissingServletRequestParameterException) {
                return doSpecialExceptionResult(HttpExceptionEnum.MISSING_REQUEST_PARAMETER_EXCEPTION, request);
            } else if (ex instanceof ServletRequestBindingException) {
                return doSpecialExceptionResult(HttpExceptionEnum.REQUEST_BINDING_EXCEPTION, request);
            } else if (ex instanceof ConversionNotSupportedException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_SUPPORTED_CONVERSION_EXCEPTION, request);
            } else if (ex instanceof TypeMismatchException) {
                return doSpecialExceptionResult(HttpExceptionEnum.TYPE_MISMATCH_EXCEPTION, request);
            } else if (ex instanceof HttpMessageNotReadableException) {
                return doSpecialExceptionResult(HttpExceptionEnum.MESSAGE_NOT_READABLE_EXCEPTION, request);
            } else if (ex instanceof HttpMessageNotWritableException) {
                return doSpecialExceptionResult(HttpExceptionEnum.MESSAGE_NOT_WRITABLE_EXCEPTION, request);
            } else if (ex instanceof MethodArgumentNotValidException) {
                return doSpecialExceptionResult(HttpExceptionEnum.NOT_VALID_METHOD_ARGUMENT_EXCEPTION, request);
            } else if (ex instanceof MissingServletRequestPartException) {
                return doSpecialExceptionResult(HttpExceptionEnum.MISSING_REQUEST_PART_EXCEPTION, request);
            } else if (ex instanceof BindException) {
                return doSpecialExceptionResult(HttpExceptionEnum.BIND_EXCEPTION, request);
            } else if (ex instanceof AsyncRequestTimeoutException) {
                return doSpecialExceptionResult(HttpExceptionEnum.ASYNC_REQUEST_TIMEOUT_EXCEPTION, request);
            }
        } catch (Exception handlerException) {
            logger.warn("Handling of [" + ex.getClass().getName() + "] pageResulted in Exception", handlerException);
        }
         return doSpecialExceptionResult(HttpExceptionEnum.UNKNOWN_EXCEPTION, request);
    }

    private ModelAndView doSpecialExceptionResult(HttpExceptionEnum httpException, HttpServletRequest request) {
        logger.warn("请求处理失败，请求url=[{}], 失败原因 : {}", request.getRequestURI(), httpException.getMessage());
        // 是不是一步ajax请求
        if ("XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))){
            ModelAndView mv = new ModelAndView();
            //todo 这里解析json 返回
            //ObjectMapper mapper = new ObjectMapper();
            //MappingJackson2JsonView view = new MappingJackson2JsonView(mapper);
            //view.setAttributesMap(mapper.convertValue(result, Map.class));
            //mv.setView(view);
            return mv;
        } else {
            return pageResult(httpException.getMessage(), "/error");
        }
    }
}

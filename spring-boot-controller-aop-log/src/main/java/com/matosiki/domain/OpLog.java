package com.matosiki.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_operation_log")
public class OpLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long ID;
    /**
     * 用户编号
     */
    @Column(name = "user_id")
    private String userID;
    /**
     * 用户名
     */
    @Column(name = "user_name")
    private String userName;
    /**
     * 请求者ip地址
     */
    @Column(name = "request_ip")
    private String requestIP;
    /**
     * 日志类型
     */
    @Column(name = "type")
    private String type;
    /**
     * 异常编码
     */
    @Column(name = "exception_code")
    private String exceptionCode;
    /**
     * 异常详情
     */
    @Column(name = "exception_detail")
    private String exceptionDetail;
    /**
     * 操作方法
     */
    @Column(name = "invocation_method")
    private String invocationMethod;
    /**
     * 操作时间
     */
    @Column(name = "invocation_date")
    private Date invocationDate;
    /**
     * 方法描述
     */
    @Column(name = "description")
    private String description;
    /**
     * 方法参数
     */
    @Column(name = "params")
    private String params;


    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRequestIP() {
        return requestIP;
    }

    public void setRequestIP(String requestIP) {
        this.requestIP = requestIP;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionDetail() {
        return exceptionDetail;
    }

    public void setExceptionDetail(String exceptionDetail) {
        this.exceptionDetail = exceptionDetail;
    }

    public String getInvocationMethod() {
        return invocationMethod;
    }

    public void setInvocationMethod(String invocationMethod) {
        this.invocationMethod = invocationMethod;
    }

    public Date getInvocationDate() {
        return invocationDate;
    }

    public void setInvocationDate(Date invocationDate) {
        this.invocationDate = invocationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }


}

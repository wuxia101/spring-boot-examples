package com.matosiki.aspect;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.matosiki.annotation.OpLogger;
import com.matosiki.domain.OpLog;
import com.matosiki.pojo.ExecutionResult;
import com.matosiki.service.OpLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.*;

@Aspect
@Component
@Order(-5)
public class OpLoggerAspect {

    @Autowired
    private OpLogService opLogService;

    /***
     *
     * 定义service切入点拦截规则，拦截SystemServiceLog注解的方法
     */
    @Pointcut("@annotation(com.matosiki.annotation.OpLogger)")
    public void serviceAspect() {
    }

    /***
     * 定义controller切入点拦截规则，拦截SystemControllerLog注解的方法
     */
    @Pointcut("@annotation(com.matosiki.annotation.OpLogger)")
    public void controllerAspect() {
    }

    /***
     * 拦截控制层的操作日志
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("controllerAspect()")
    public Object recordLog(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("========进来了=====================");
        OpLog operationLog = new OpLog();
        Object proceed = null;
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        //设置用户操作信息
        getUserInformationWithRedisByCookie(operationLog, request);
        //获取请求的ip
        String ip = request.getRemoteAddr();
        operationLog.setRequestIP(ip);
        //获取执行的方法名
        operationLog.setInvocationMethod(joinPoint.getSignature().getName());


        //获取方法执行前时间
        Date date = new Date();
        operationLog.setInvocationDate(date);

        proceed = joinPoint.proceed();
        System.out.println("========处理了=====================");
        //提取controller中ExecutionResult的属性
        ExecutionResult result = null;
        if (proceed instanceof String) {
            System.out.println(proceed);
            ObjectMapper mapper = new ObjectMapper();
            ExecutionResult result1 = mapper.convertValue(proceed, new TypeReference<ExecutionResult>() {
            });
            result = result1;
        } else {
            result = (ExecutionResult) proceed;
        }
        if (result.getCode().equals(ExecutionResult.ReturnCode.RES_SUCCESS)) {
            //设置操作信息
            operationLog.setType(ExecutionResult.ReturnCode.RES_SUCCESS.code);
            //获取执行方法的注解内容
            operationLog.setDescription("用户：" + operationLog.getUserName() + "执行==>" + getControllerMethodDescription(joinPoint) + ":" + result.getMessage());
        } else {
            operationLog.setType(ExecutionResult.ReturnCode.RES_SUCCESS.code);
            operationLog.setExceptionCode(result.getMessage());
        }
        setActionMethodAndParameter(joinPoint, operationLog);
        opLogService.saveLog(operationLog);
        return proceed;
    }

    //异常处理
    @AfterThrowing(pointcut = "controllerAspect()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable e) throws Throwable {
        OpLog opLog = new OpLog();
        Object proceed = null;
        //获取session中的用户
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        //request.getSession().getAttribute("user");
        //opLog.setUserId("vesus");
        getUserInformationWithRedisByCookie(opLog, request);
        //获取请求的ip
        String ip = request.getRemoteAddr();
        opLog.setRequestIP(ip);

        //opLog.setActionMethod(getActionMethod(joinPoint));
        setActionMethodAndParameter(joinPoint, opLog);
        opLog.setType(ExecutionResult.ReturnCode.RES_FAILED.code);
        opLog.setExceptionCode(e.getClass().getName());
        opLog.setExceptionDetail(e.getMessage());
        //保存系统操作日志
        opLogService.saveLog(opLog);
    }


    /**
     * 功能描述:
     * 设置操作用户的信息
     *
     * @author: wuxia
     * @date: `11:31` 2018/6/4
     * @param:
     * @return:
     */
    private void getUserInformationWithRedisByCookie(OpLog operationLog, HttpServletRequest request) {
        String token = request.getHeader("token");
        //获取session中的用户
        //
        //if (rwToken != null) {
        //    //todo 添加用户信息
        //    operationLog.setUserId(String.valueOf(rwToken.getUser_id()));
        //    operationLog.setUserPhone(rwUser.getPhone());
        //    operationLog.setUserName(rwUser.getUername());
        //    return;
        //}
        operationLog.setUserName("随机用户");
    }

    /**
     * 功能描述:
     * 设置方法名 和调用参数名称与值
     *
     * @author: wuxia
     * @date: 14:09 2018/6/4
     */

    public void setActionMethodAndParameter(JoinPoint point, OpLog log) throws Exception {
        //所有参数值
        List<String> parameterValues = getActionParameters(point);
        //所有参数名称
        List<String> parameterNames = new ArrayList<>(10);
        //获取连接点目标类名
        String targetName = point.getTarget().getClass().getName();
        //获取连接点签名的方法名
        String methodName = point.getSignature().getName();
        //获取连接点参数
        Object[] args = point.getArgs();
        //根据连接点类的名字获取指定类
        Class targetClass = Class.forName(targetName);
        //获取类里面的方法
        Method[] methods = targetClass.getMethods();
        String actionType = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == args.length) {
                    actionType = method.getAnnotation(OpLogger.class).invocationType().name();
                    String[] parameterNameArrays = new LocalVariableTableParameterNameDiscoverer().getParameterNames(method);
                    parameterNames = Arrays.asList(parameterNameArrays);
                    break;
                }
            }
        }
        Map<String, String> parameterMaps = new HashMap<>();
        if (parameterNames.size() == parameterValues.size()) {
            for (int i = 0; i < parameterValues.size(); i++) {
                parameterMaps.put(parameterNames.get(i), parameterValues.get(i));
            }
        } else {
            parameterMaps.put("参数异常", "参数个数与定义数量不一致");
        }
        log.setInvocationMethod(actionType);
        log.setParams(new ObjectMapper().writeValueAsString(parameterMaps));
        return;
    }

    /**
     * 功能描述:
     * 获取方法传递的参数，只打印字符串和数字类型
     * TODO 这里由于无法获取参数名称，暂时不能组成键值对展示传递参数，而其他类型参数转字符串太长并没实际意义。为空或者为其他对象的都使用对象值省略的方式
     *
     * @author: wuxia
     * @date: 13:52 2018/6/5
     */
    private List<String> getActionParameters(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        List<String> list = new ArrayList<>();
        for (Object param : params) {
            if (param instanceof String) {
                list.add(param + "");
            } else if (param instanceof Integer) {
                list.add(param + "");
            } else if (param instanceof Double) {
                list.add(param + "");
            } else if (param instanceof Float) {
                list.add(param + "");
            } else {
                list.add("对象值省略");
            }
        }
        return list;
    }

    /***
     * 获取controller的操作信息
     * @param point
     * @return
     */
    public String getControllerMethodDescription(ProceedingJoinPoint point) throws Exception {
        //获取连接点目标类名
        String targetName = point.getTarget().getClass().getName();
        //获取连接点签名的方法名
        String methodName = point.getSignature().getName();
        //获取连接点参数
        Object[] args = point.getArgs();
        //根据连接点类的名字获取指定类
        Class targetClass = Class.forName(targetName);
        //获取类里面的方法
        Method[] methods = targetClass.getMethods();
        String description = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == args.length) {
                    description = method.getAnnotation(OpLogger.class).description();
                    break;
                }
            }
        }
        return description;
    }


}

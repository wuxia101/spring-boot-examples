package com.matosiki;

import com.matosiki.consumer.service.CityService;
import com.matosiki.domain.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringBootDubboConsumerApplication {

    @Autowired
    CityService cityService;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDubboConsumerApplication.class, args);
    }


    @RequestMapping("index")
    public City index() {
        return cityService.findOne();
    }

}

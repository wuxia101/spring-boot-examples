package com.matosiki.consumer.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.matosiki.api.CityDubboService;
import com.matosiki.domain.City;
import org.springframework.stereotype.Component;

@Component
public class CityService {

    @Reference(version = "1.0.0")
    CityDubboService cityDubboService;

    public void printCity() {
        String cityName = "杭州";
        City city = cityDubboService.findCityByName(cityName);
        System.out.println(city.toString());
    }

    public City findOne() {
        return cityDubboService.findCityByName("杭州");
    }
}

package com.matosiki;

import com.rometools.rome.feed.WireFeed;
import com.rometools.rome.feed.atom.*;
import com.rometools.rome.feed.module.Module;
import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Description;
import com.rometools.rome.feed.rss.Image;
import com.rometools.rome.feed.rss.Item;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndPerson;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.jdom2.Element;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRssApplicationTests {
    static final String RSSURL = "http://feed.cnblogs.com/blog/u/378562/rss";


    @Test
    public void readRss() throws Exception {
        SyndFeed syndFeed = new SyndFeedInput().build(new XmlReader(new URL(RSSURL)));
        // 第一种 读取wirefeed
        WireFeed wireFeed = syndFeed.createWireFeed();
        wireFeed.setEncoding("UTF-8");
        List<Module> modules = wireFeed.getModules();
        List<Element> elements = wireFeed.getForeignMarkup();
        for (Element element : elements) {
            System.out.println(element.getName());
        }
        //第二种
        for (SyndEntry entry : syndFeed.getEntries()) {
            System.out.println(entry.getContents());
        }
    }


    /**
     * 发布rss
     */

    @Test
    public  void rss(){

        Channel channel = new Channel();
        channel.setFeedType("rss_2.0");
        channel.setTitle("Matosiki Feed");
        channel.setDescription("最新技术的不同文章");
        channel.setLink("http://www.matosiki.com");
        channel.setUri("http://www.matosiki.com");
        channel.setGenerator("iki");

        Image image = new Image();
        image.setUrl("http://img.matosiki.site/");
        image.setTitle("404");
        image.setHeight(32);
        image.setWidth(32);
        channel.setImage(image);

        Date postDate = new Date();
        channel.setPubDate(postDate);

        Item item = new Item();
        item.setAuthor("matosiki");
        item.setLink("http://www.matosiki.site/page/3/");
        item.setTitle("java使用Guava进行字符串处理");
        item.setUri("http://www.matosiki.site/2018/11/03/java/%E5%9F%BA%E7%A1%80/java%E4%BD%BF%E7%94%A8Guava%E8%BF%9B%E8%A1%8C%E5%AD%97%E7%AC%A6%E4%B8%B2%E5%A4%84%E7%90%86/");
        item.setComments("从字符串中删除特定的字符");

        com.rometools.rome.feed.rss.Category category = new com.rometools.rome.feed.rss.Category();
        category.setValue("CORS");
        item.setCategories(Collections.singletonList(category));

        Description descr = new Description();
        descr.setValue("<h1>description</h1>");
        item.setDescription(descr);
        item.setPubDate(postDate);

        channel.setItems(Collections.singletonList(item));
        System.out.println(channel);
    }

    @Test
    public void atom(){
        Feed feed = new Feed();
        feed.setFeedType("atom_1.0");
        feed.setTitle("Leftso");
        feed.setId("http://www.matosiki.com/");

        Content subtitle = new Content();
        subtitle.setType("text/plain");
        subtitle.setValue("最新技术的不同文章");
        feed.setSubtitle(subtitle);

        Date postDate = new Date();
        feed.setUpdated(postDate);

        Entry entry = new Entry();

        Link link = new Link();
        link.setHref("http://www.matosiki.site/2018/11/03");
        entry.setAlternateLinks(Collections.singletonList(link));
        SyndPerson author = new Person();
        author.setName("Matosiki");
        entry.setAuthors(Collections.singletonList(author));
        entry.setCreated(postDate);
        entry.setPublished(postDate);
        entry.setUpdated(postDate);
        entry.setId("http://www.matosiki.site/2018/11/03");
        entry.setTitle("Spring boot 入门(一)环境搭建以及第一个应用");

        Category category = new Category();
        category.setTerm("CORS");
        entry.setCategories(Collections.singletonList(category));

        Content summary = new Content();
        summary.setType("text/plain");
        summary.setValue("两者没有必然的联系,但是spring boot可以看作为spring MVC的升级版."
                + " <a rel=\"nofollow\" href=\"http://www.leftso.com/blog/64.html/\">Spring boot 入门(一)环境搭建以及第一个应用</a>发布在 <a rel=\"nofollow\" href=\"http://www.leftso.com\">Leftso</a>.");
        entry.setSummary(summary);

        feed.setEntries(Collections.singletonList(entry));
        //参加这里关于不同的新话题
        System.out.println(feed);
    }



}

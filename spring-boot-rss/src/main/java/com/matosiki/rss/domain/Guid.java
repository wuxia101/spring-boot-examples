package com.matosiki.rss.domain;

import java.io.Serializable;

public class Guid implements Serializable {
    private boolean permaLink;
    private String value;

    public boolean isPermaLink() {
        return permaLink;
    }

    public void setPermaLink(boolean permaLink) {
        this.permaLink = permaLink;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

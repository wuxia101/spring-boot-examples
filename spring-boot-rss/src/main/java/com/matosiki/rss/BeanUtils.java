package com.matosiki.rss;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class BeanUtils {

    /**
     * 复制对象属性工具类
     *
     * @param dest
     * @param orig
     * @param <T>
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static <T> void copyProperties(final T dest, final T orig) throws IllegalAccessException, InstantiationException {
        if (dest == null || orig == null) {
            return;
        }
        Class<?> destClass = dest.getClass();
        Class<?> origClass = orig.getClass();
        Field[] fields = origClass.getDeclaredFields();
        for (Field field : fields) {
            String origFieldName = field.getName();
            Field declaredField;
            try {
                //没有的字段跳过
                declaredField = destClass.getDeclaredField(origFieldName);
            } catch (NoSuchFieldException e) {
                continue;
            }
            declaredField.setAccessible(true);
            field.setAccessible(true);
            String fieldType = field.getType().getTypeName();
            int a = field.getType().isPrimitive() ? 1 : 0;
            int b = declaredField.getType().isPrimitive() ? 1 : 0;
            // 不能一个是原生对象一个是抱装类对象
            if ((a + b) == 1) {
                continue;
            }
            if (declaredField != null && fieldType.equals(declaredField.getType().getName())) {
                //如果是集合
                if ("java.util.List".equals(fieldType)) {
                    List list = (List) declaredField.get(dest);
                    ArrayList<Object> res = new ArrayList<>();
                    Type genericType = field.getGenericType();
                    if (genericType == null) continue;
                    // 如果是泛型参数的类型
                    //System.out.println(genericType.getTypeName());
                    if (genericType instanceof ParameterizedType) {
                        ParameterizedType pt = (ParameterizedType) genericType;
                        //得到泛型里的class类型对象
                        Class<?> genericClazz = (Class<?>) pt.getActualTypeArguments()[0];
                        Object target = genericClazz.newInstance();
                        for (Object o : list) {
                            copyProperties(o, target);
                            res.add(target);
                        }
                        field.set(orig, res);
                    }
                    continue;
                }
                Object value = declaredField.get(dest);
                field.set(orig, value);
            } else {
                field.set(orig, field.getType().newInstance());
                copyProperties(declaredField.get(dest), field.get(orig));
            }
            field.setAccessible(false);
            declaredField.setAccessible(false);
        }
    }

}

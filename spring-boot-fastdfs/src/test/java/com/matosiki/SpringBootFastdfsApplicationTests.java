package com.matosiki;

import ch.qos.logback.core.util.ContentTypeUtil;
import com.matosiki.client.FastDFSClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootFastdfsApplicationTests {
    @Autowired
    FastDFSClient fastDFSClient;

    @Autowired
    private Environment environment;

    @Test
    public void contextLoads() throws Exception {

        System.out.println("springboot 启动测试。。。。。。。。");

    }

    @Test
    public void readFileTest() throws Exception {
        File file = ResourceUtils.getFile("classpath:application.properties");
        Assert.assertTrue(file.exists());

    }
    @Test
    public void uploadTest() throws Exception {
        File file = ResourceUtils.getFile("classpath:application.properties");
        MultipartFile mulFile = new MockMultipartFile(
                "application.properties", //文件名
                "application.properties", //originalName 相当于上传文件在客户机上的文件名
                ContentTypeUtil.getSubType("APPLICATION_OCTET_STREAM"),
                new FileInputStream(file) //文件流
        );

        String filepath = fastDFSClient.uploadFileWithMultipart(mulFile);

        String token = FastDFSClient.getToken(filepath, environment.getProperty("fastdfs.http_secret_key"));
        System.out.println(token);

        String file_server_addr = "redirect:http://" + environment.getProperty("file_server_addr") + "/" + filepath + "?token=" + token;

        System.out.println(file_server_addr);
    }

    @Test
    public void downloadTest() throws Exception {


    }

    @Test
    public void deleteTest() throws Exception {


    }

}

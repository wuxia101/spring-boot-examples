package com.matosiki.listener;


import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class CustomAuditor implements AuditorAware<String> {
    private String username;
    private String userId;

    //@Override
    //public String getCurrentAuditor() {
    //    return this.username;
    //}

    public CustomAuditor() {
        super();
    }

    public CustomAuditor(String username) {
        this.username = username;
    }

    public CustomAuditor(String username, String userId) {
        this.username = username;
        this.userId = userId;
    }

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.empty();
    }
}

package com.matosiki;

import com.matosiki.audit.AuditOperationRecord;
import com.matosiki.listener.CustomAuditor;
import com.matosiki.listener.CustomInterceptor;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
public class SpringBootJpaInterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaInterceptorApplication.class, args);
    }

    @Bean
    public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {
        //return hemf.unwrap(SessionFactory.class);
        return hemf.getSessionFactory();
    }

    //@Autowired
    //private EntityManagerFactory entityManagerFactory;
    //
    //@Bean
    //public SessionFactory getSessionFactory() {
    //    if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
    //        throw new NullPointerException("factory is not a hibernate factory");
    //    }
    //    return entityManagerFactory.unwrap(SessionFactory.class);
    //}
    //
    //@Bean
    //AuditOperationRecordAware auditOperationRecordAware() {
    //    return new AuditOperationRecordAwareImpl();
    //}


    @Bean
    AuditorAware<String> auditorProvider() {
        return new CustomAuditor();
    }


    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    AuditOperationRecord auditOperationRecord;

    @Autowired
    CustomInterceptor interceptor;

    @PostConstruct
    public void registerInterceptor() {
        interceptor.setAuditOperationRecord(auditOperationRecord);
        sessionFactory.withOptions().interceptor(interceptor);
        System.out.println("===========================postConstruction=========================");
        System.out.println("===========================postConstruction=========================");
        System.out.println("===========================postConstruction=========================");
        System.out.println("===========================postConstruction=========================");
    }

}

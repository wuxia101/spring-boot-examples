package com.matosiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApiSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootApiSwaggerApplication.class, args);
    }
}

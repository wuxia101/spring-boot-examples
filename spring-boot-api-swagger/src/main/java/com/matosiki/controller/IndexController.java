package com.matosiki.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.matosiki.domain.Note;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "index", tags = "导航接口模块")
@RequestMapping("index")
public class IndexController {
    @ApiOperation(value = "index 接口测试", tags = "导航")
    @GetMapping
    public String index() {
        return "wo ho yo go za yi ma si ";
    }


    /**
     * showdoc
     *
     * @param note
     *         必选 Note 笔记对象
     * @return {"error_code":0,"data":{"uid":"1","username":"12154545","name":"吴系挂","groupid":2,"reg_time":"1436864169","last_login_time":"0"}}
     * @catalog 测试文档 /用户相关
     * @title 用户登录
     * @description 用户登录的接口
     * @method get
     * @url https://www.showdoc.cc/home/user/login
     * @return_param groupid int 用户组 id
     * @return_param name string 用户昵称
     * @remark 这里是备注信息
     * @number 99
     */
    @ApiOperation(value = "插入笔记记录", tags = "笔记")
    @PostMapping("insertNote")
    @ApiImplicitParam(name = "note", value = "Note", required = true, dataType = "Note")
    public String insertNote(@RequestBody Note note) throws Exception {
        System.out.println(new ObjectMapper().writeValueAsString(note));
        return "success";
    }
}

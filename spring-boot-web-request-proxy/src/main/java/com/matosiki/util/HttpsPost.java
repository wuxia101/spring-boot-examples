package com.matosiki.util;


import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class HttpsPost {
	
	/**
	 * 忽略HostName
	 */
	private static HostnameVerifier ignoreHostnameVerifier = new HostnameVerifier() {
		public boolean verify(String s, SSLSession sslsession) {
			return true;
		}
	};

	/**
	 * Ignore Certification
	 */
	private static TrustManager ignoreCertificationTrustManger = new X509TrustManager() {

		private X509Certificate[] certificates;

		public void checkClientTrusted(X509Certificate certificates[],
				String authType) throws CertificateException {
			if (this.certificates == null) {
				this.certificates = certificates;
			}

		}

		public void checkServerTrusted(X509Certificate[] ax509certificate,
				String s) throws CertificateException {
			if (this.certificates == null) {
				this.certificates = ax509certificate;
			}

			
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

	};
	
    public static  String post(String myURL,String params){
    	
    	HttpsURLConnection.setDefaultHostnameVerifier(ignoreHostnameVerifier);
		HttpsURLConnection httpsConn=null;
		String contant="";
		BufferedReader reader=null;

        try {
			httpsConn = (HttpsURLConnection) ((new URL(myURL)).openConnection());
			
			TrustManager[] tm = { ignoreCertificationTrustManger };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
				 
			SSLSocketFactory ssf = sslContext.getSocketFactory();
			httpsConn.setSSLSocketFactory(ssf); 
			
			
			httpsConn.setDoOutput(true);
			httpsConn.setDoInput(true);
			httpsConn.setRequestMethod("POST");
			httpsConn.setUseCaches(false);
			httpsConn.setInstanceFollowRedirects(true);
//			httpsConn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			httpsConn.setRequestProperty("Content-Type", "text/html;charset=UTF-8");
			httpsConn.connect();
	        
	        DataOutputStream out=new DataOutputStream(httpsConn.getOutputStream());
			String content=URLEncoder.encode(params, "utf-8");
			
			out.writeBytes(content);
			out.flush();
			out.close();
			
			reader=new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
			String line;
			
			while((line=reader.readLine())!=null){
				contant+=line;
				System.out.println(URLDecoder.decode(line, "utf-8"));
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			try {
				if (reader != null) {
					reader.close();
				}
				if(httpsConn != null){
					httpsConn.disconnect();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
//			断开连接
		}
        
        return contant;
	}

}

package com.matosiki.util;


import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * JSON工具类
 */
public class JSONUtil {

    /**
     * 输出json
     *
     * @param response
     */
    public static void toObjectJson(HttpServletResponse response, String json) {
        try {
            response.setContentType("application/json; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            out.print(json);
            out.flush();
            out.close();
        } catch (Exception e) {

        }
    }


}

package com.matosiki.controller;


import com.matosiki.util.HttpsClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
public class WebController {

    protected final Logger logger = LoggerFactory.getLogger(WebController.class);

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/get/")
    @ResponseBody
    public byte[] get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String reqURL = request.getRequestURI();
        String orgURI = StringUtils.replace(request.getQueryString(), "requestURI=", "");
        logger.debug("=======>>>>> get请求地址：" + orgURI + " <<<<<==========");
        byte[] forObject = restTemplate.getForObject(orgURI, byte[].class);
        return forObject;
    }
    @RequestMapping("/post/")
    @ResponseBody
    public byte[] post(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String reqURL = request.getRequestURI();
        String orgURI = StringUtils.replace(request.getQueryString(), "requestURI=", "");


        logger.debug("=======>>>>> post请求地址：" + orgURI + " <<<<<==========");



        byte[] forObject = restTemplate.postForObject(orgURI, request.getParameterMap(), byte[].class, request.getParameterMap());
        return forObject;
    }

    @RequestMapping("/orc/")
    @ResponseBody
    public String orc(HttpServletRequest request, HttpServletResponse response, @RequestBody String entity) throws ServletException, IOException {
        String reqURL = request.getRequestURI();
        String orgURI = StringUtils.replace(request.getQueryString(), "requestURI=", "");


        logger.debug("=======>>>>> post请求地址：" + orgURI + " <<<<<==========");
        Map<String, String[]> parameterMap = request.getParameterMap();

//        System.out.println(entity);
        String result = HttpsClientUtil.doPost(orgURI, entity, "utf-8");
        logger.info(result);
        return result;
    }



}

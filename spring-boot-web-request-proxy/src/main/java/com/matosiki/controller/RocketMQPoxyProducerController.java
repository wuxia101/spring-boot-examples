package com.matosiki.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.matosiki.util.JSONUtil;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/poxy")
public class RocketMQPoxyProducerController {
    protected final Logger logger = LoggerFactory.getLogger(RocketMQPoxyProducerController.class);

    @RequestMapping("creditreport/taobao-return-back")
    @ResponseBody
    public String createTaobaoReturnCallback(HttpServletRequest request, HttpServletResponse response) {
//        String record = request.getParameter("record");
//        String mobilePhone = request.getParameter("mobilePhone");
//        String clientType = request.getParameter("clientType");
//        String deviceId = request.getParameter("deviceId");
//        String userId = request.getParameter("userId");
//        String token = request.getParameter("token");
//            HashMap<String, String> maps = new HashMap<>();
        //公信宝查询任务token
        try {
            Map<String, String[]> parameterMap = request.getParameterMap();
             return   proxyProducer("taobao-return-back", parameterMap);
        } catch (MQClientException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @RequestMapping("test")
    @ResponseBody
    public String test(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map<String, String[]> parameterMap = request.getParameterMap();
            proxyProducer("taobao-return-back", parameterMap);
        } catch (MQClientException | InterruptedException e) {
            e.printStackTrace();
        }
        return "{\"message\":\"success\"}";
    }

    @RequestMapping(value = "creditreport/taobao-notify-back", method = RequestMethod.POST)
    @ResponseBody
    public void createTaobaoNotifyCallback(HttpServletRequest request,
                                           HttpServletResponse response, @RequestBody JSONObject jsonObject) {
        HashMap<String, String> resMap = new HashMap<String, String>();

        try {
            proxyProducer("taobao-notify-back", jsonObject);
        } catch (MQClientException | InterruptedException e) {
            e.printStackTrace();
            resMap.put("retCode", "2");
            resMap.put("retMsg", "失败");
            JSONUtil.toObjectJson(response, JSON.toJSONString(resMap));
            return;
        }
        resMap.put("retCode", "1");
        resMap.put("retMsg", "成功");
        JSONUtil.toObjectJson(response, JSON.toJSONString(resMap));
    }


    private String proxyProducer(String tags, Object o) throws MQClientException, InterruptedException {
        DefaultMQProducer producer = new DefaultMQProducer("proxy_interface_group");
        producer.setNamesrvAddr("172.26.205.168:9876;172.26.205.165:9876");
        String  result ="";
        producer.start();
        try {
            Message msg = new Message("proxyInterfaceTopic", tags, JSONObject.toJSONString(o).getBytes(RemotingHelper.DEFAULT_CHARSET));
            SendResult sendResult = producer.send(msg);
            logger.info("%s%n", sendResult);
            result = sendResult.toString();
        } catch (Exception e) {
            e.printStackTrace();
            result = e.getMessage();
            Thread.sleep(1000);
        }
        producer.shutdown();
        return result;

    }


}

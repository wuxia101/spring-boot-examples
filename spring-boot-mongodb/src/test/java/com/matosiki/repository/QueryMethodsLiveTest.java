package com.matosiki.repository;


import com.matosiki.config.MongoConfig;
import com.matosiki.domain.City;
import com.matosiki.domain.EmailAddress;
import com.matosiki.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MongoConfig.class)
public class QueryMethodsLiveTest extends BaseQueryLiveTest {

    @Test
    public void givenUsersExist_whenFindingUsersByName_thenUsersAreFound() {
        User user = new User();
        City city = new City();
        city.setId(12341234L);
        city.setCityName("浙江");
        city.setProvinceId(123123L);
        city.setDescription("江南好地方！");
        EmailAddress emailAddress = new EmailAddress();
        emailAddress.setId("109876543");
        emailAddress.setValue("wx11055@163.com");
        user.setCity(city);
        user.setEmailAddress(emailAddress);
        user.setName("Eric");
        user.setAge(45);
        mongoOps.insert(user);
        user = new User();
        user.setName("Antony");
        //user.setAge(55);
        mongoOps.insert(user);
        List<User> users = userRepository.findByName("Eric");
        assertThat(users.size(), is(1));
    }

    @Test
    public void givenUsersExist_whenFindingUsersWithAgeCreaterThanAndLessThan_thenUsersAreFound() {
        User user = new User();
        user.setAge(20);
        user.setName("Jon");
        mongoOps.insert(user);

        user = new User();
        user.setAge(50);
        user.setName("Jon");
        mongoOps.insert(user);

        user = new User();
        user.setAge(33);
        user.setName("Jim");
        mongoOps.insert(user);

        List<User> users = userRepository.findByAgeBetween(26, 40);
        assertThat(users.size(), is(1));
    }

    @Test
    public void givenUsersExist_whenFindingUserWithNameStartWithA_thenUsersAreFound() {
        User user = new User();
        user.setName("Eric");
        user.setAge(45);
        mongoOps.insert(user);

        user = new User();
        user.setName("Antony");
        user.setAge(33);
        mongoOps.insert(user);

        user = new User();
        user.setName("Alice");
        user.setAge(35);
        mongoOps.insert(user);

        List<User> users = userRepository.findByNameStartingWith("A");
        assertThat(users.size(), is(2));
    }

    @Test
    public void givenUsersExist_whenFindingUserWithNameEndWithC_thenUsersAreFound() {
        User user = new User();
        user.setName("Eric");
        user.setAge(45);
        mongoOps.insert(user);

        user = new User();
        user.setName("Antony");
        user.setAge(33);
        mongoOps.insert(user);

        user = new User();
        user.setName("Alice");
        user.setAge(35);
        mongoOps.insert(user);

        List<User> users = userRepository.findByNameEndingWith("c");

        assertThat(users.size(), is(1));
    }

    @Test
    public void givenUsersExist_whenFindingUsersAndSortThem_thenUsersAreFoundAndSorted() {
        User user = new User();
        user.setName("Eric");
        user.setAge(45);
        mongoOps.insert(user);

        user = new User();
        user.setName("Antony");
        user.setAge(33);
        mongoOps.insert(user);

        user = new User();
        user.setName("Alice");
        user.setAge(35);
        mongoOps.insert(user);

        List<User> users = userRepository.findByNameLikeOrderByAgeAsc("A");

        assertThat(users.size(), is(2));
    }
}
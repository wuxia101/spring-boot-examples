package com.matosiki;

import com.matosiki.domain.City;
import com.matosiki.handler.CityHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootMongodbApplicationTests {
    @Autowired
    private CityHandler cityHandler;
    @Test
    public void contextLoads() {
        City city = new City();
        city.setCityName("杭州");
        city.setDescription("上有天堂，下有苏杭");
        city.setProvinceId(100000L);
        city.setId(1L);
        cityHandler.save(city);
    }

}

package com.matosiki.handler;

import com.matosiki.domain.City;
import com.matosiki.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CityHandler {

    private final CityRepository cityRepository;

    @Autowired
    public CityHandler(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public City save(City city) {
        return cityRepository.save(city);
    }

    public City findCityById(Long id) {
        return cityRepository.findById(id).get();
    }

    public List<City> findAllCity() {
        return cityRepository.findAll();
    }

    public City modifyCity(City city) {
        return cityRepository.save(city);
    }

    public boolean deleteCity(Long id) {
        cityRepository.findById(id);
        return cityRepository.existsById(id);
    }
}
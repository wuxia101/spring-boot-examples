package com.matosiki.controller;

import com.matosiki.domain.City;
import com.matosiki.handler.CityHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/city")
public class CityWebController {

    @Autowired
    private CityHandler cityHandler;

    @GetMapping(value = "/{id}")
    public City findCityById(@PathVariable("id") Long id) {
        return cityHandler.findCityById(id);
    }

    @GetMapping("findAllCity")
    public List<City> findAllCity() {
        return cityHandler.findAllCity();
    }

    @PostMapping("saveCity")
    public City saveCity(@RequestBody City city) {
        return cityHandler.save(city);
    }

    @PutMapping("modifyCity")
    public City modifyCity(@RequestBody City city) {
        return cityHandler.modifyCity(city);
    }

    @DeleteMapping(value = "deleteCity/{id}")
    public boolean deleteCity(@PathVariable("id") Long id) {
        return cityHandler.deleteCity(id);
    }
}

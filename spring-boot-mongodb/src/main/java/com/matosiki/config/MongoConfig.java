package com.matosiki.config;

import com.matosiki.event.CascadeSaveMongoEventListener;
import com.matosiki.event.UserCascadeSaveMongoEventListener;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ConfigurationProperties(prefix = "mongodb")
@PropertySource("classpath:/mongodb.properties")
@EnableMongoRepositories(basePackages = "com.matosiki.repository")
public class MongoConfig extends AbstractMongoConfiguration {

    //private final List<Converter<?, ?>> converters = new ArrayList<Converter<?, ?>>();

    @Value("${mongodb.username}")
    private String username;

    @Value("${mongodb.password}")
    private String password;

    @Value("${mongodb.host}")
    private String host;

    @Value("${mongodb.database}")
    private String dbname;

    @Value("${mongodb.port}")
    private Integer port;

    @Override
    protected String getDatabaseName() {
        return dbname;
    }

    @Override
    public MongoClient mongoClient() {
        return new MongoClient(host, port);
    }

    @Override
    public String getMappingBasePackage() {
        return "com.matosiki";
    }

    @Bean
    public UserCascadeSaveMongoEventListener userCascadingMongoEventListener() {
        return new UserCascadeSaveMongoEventListener();
    }

    @Bean
    public CascadeSaveMongoEventListener cascadingMongoEventListener() {
        return new CascadeSaveMongoEventListener();
    }


    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
    }


}
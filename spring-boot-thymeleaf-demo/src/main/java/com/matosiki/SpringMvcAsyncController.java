package com.matosiki;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.WebAsyncTask;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;

@Controller
public class SpringMvcAsyncController {


    /**
     * 方法一、   异步回调
     *
     * @return
     */
    @RequestMapping("/callable")
    @ResponseBody
    public Callable<Good> callable() {
        System.out.println("开始执行任务!");
        return () -> {
            Thread.sleep(3000L);
            return new Good(1, "商品", new Date().toString());
        };
    }

    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);

    //private DelayQueue delayQueue = new DelayQueue();



    /**
     * 方法二  DeferredResult
     * @return
     */

    //2. DeferredResult 异步功能
    @RequestMapping(value = "/deferred")
    public DeferredResult<ModelAndView> deferred() {
        DeferredResult<ModelAndView> deferredResult = new DeferredResult<>();
        System.out.println("线程 : " + Thread.currentThread().getId());

        scheduler.schedule(() -> {

            try {
                Thread.sleep(new Random().nextInt(10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ModelAndView mav = new ModelAndView("deferred");
            mav.addObject("result", "deferred异步执行完成");
            System.out.println("异步调用执行完成, 线程 : " + Thread.currentThread().getId());
            deferredResult.setResult(mav);  //设置结果返回
        },  5 , TimeUnit.SECONDS);


        deferredResult.onTimeout(() -> {
            ModelAndView mav = new ModelAndView("deferred");
            mav.addObject("result", "deferred异步调用执行超时");
            System.out.println("异步调用执行超时！线程 : " + Thread.currentThread().getId());
            deferredResult.setResult(mav);
        });
        System.out.println("主线程执行完成! 线程 : " + Thread.currentThread().getId());
        return deferredResult;
    }

    /**
     * 方法三
     * @return
     */
    @ResponseBody
    @RequestMapping("/sseEmitter")
    public SseEmitter sseEmitter() {
         SseEmitter sseEmitter = new SseEmitter();

        scheduler.schedule(()->{
            try {
                Thread.sleep(2000L);
                sseEmitter.send(System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            sseEmitter.complete();
        },2L,TimeUnit.SECONDS);
        return sseEmitter;
    }

    /**
     * 方法四
     * @return
     */
    @RequestMapping(value = "/asyncTask")
    public WebAsyncTask asyncTask() {
        long start = System.currentTimeMillis();
        System.out.println("线程 : " + Thread.currentThread().getId());
        ModelAndView mv = new ModelAndView("asyncTask");
        WebAsyncTask asyncTask = new WebAsyncTask(5000, () -> {
            Thread.sleep((long) new Random().nextInt(6000));
            mv.addObject("result", "asyncTask执行成功");
            System.out.println("异步调用执行完成, 线程 : " + Thread.currentThread().getId()+"time:" +String.valueOf(System.currentTimeMillis() - start));
            return mv;
        });
        asyncTask.onTimeout(
                (Callable<ModelAndView>) () -> {
                    mv.addObject("result", "执行超时");
                    System.out.println("执行超时 , 线程：" + Thread.currentThread().getId());
                    return mv;
                }
        );
        System.out.println("主线程执行完成! 线程 : " + Thread.currentThread().getId());
        return asyncTask;
    }

    /**
     * 方法五
     * @return
     */
    @GetMapping("/download")
    public StreamingResponseBody handle() {
        return outputStream -> {
            // write...
        };
    }


    @ExceptionHandler(Exception.class)
    public ModelAndView handleAllException(Exception ex) {
        ModelAndView model = new ModelAndView("error");
        model.addObject("result", ex.getMessage());
        return model;
    }

}

class Good {
    Integer id;
    String name;
    String createTime;

    public Good(Integer id, String name, String createTime) {
        this.id = id;
        this.name = name;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
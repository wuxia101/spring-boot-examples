package com.matosiki.order;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Order implements Serializable {

    private String id;
    private String name;
    private OrderStatus status;
    private LocalDateTime createTime;
    private long expire;
    private LocalDateTime expireTime;

    public Order(String id, String name, OrderStatus status, long expire) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.expire = expire;
        this.createTime = LocalDateTime.now();
        this.expireTime = LocalDateTime.now().plusSeconds(expire);
    }


    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    enum OrderStatus {
        UNPAY, PAYED, EXPIRED
    }
}


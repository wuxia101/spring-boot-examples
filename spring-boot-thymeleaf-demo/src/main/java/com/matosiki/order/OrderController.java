package com.matosiki.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
public class OrderController {

    public static ConcurrentHashMap<String, Order> db = new ConcurrentHashMap(5);

    @Autowired
    @Qualifier("delayQueueOrderService")
    private DelayQueueOrderService delayQueueOrderService;

    /**
     * 生成三个订单
     * @return
     */
    @RequestMapping("/order")
    public Map createOrder() {
        for (int i = 0; i < 3; i++) {
            String id = UUID.randomUUID().toString().replaceAll("-", "");
            int expireTime = new Random().nextInt(20) + 5;
            Order order = new Order(id, "神秘博士1排" + i + "座,过期时间" + expireTime+"秒", Order.OrderStatus.UNPAY, expireTime);
            // 模拟放入延迟队列
            delayQueueOrderService.orderDelay(expireTime, order);
            // 模拟放入数据库
            db.put(id, order);
        }
        return db;
    }

    /**
     * 获取库存中的订单
     * @return
     */
    @RequestMapping("/remain")
    public Map remain() {
        return db;
    }

    /**
     * 支付某个订单
     * @param id
     * @return
     */
    @RequestMapping("/pay")
    public Map pay(String id) {
        Order order = db.get(id);
        order.setStatus(Order.OrderStatus.PAYED);
        db.put(id, order);
        return db;
    }

}

package com.matosiki.order;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

@Service
@Qualifier("delayQueueOrderService")
public class DelayQueueOrderService {

    private static DelayQueue<ItemVo<Order>> delayQueue = new DelayQueue<>();

    public void orderDelay(long expireTime, Order order) {
        ItemVo<Order> itemVo = new ItemVo<>(expireTime * 1000, order);
        delayQueue.put(itemVo);
    }

    private class TakeOrder implements Runnable {
        // todo 可以在此添加构造器 加入其他server处理
        //private ProcessService service;
        //
        //public TakeOrder(ProcessService service) {
        //    this.service = service;
        //}

        @Override
        public void run() {
            System.out.println("开始获取延时队列过期数据");
            while (!Thread.currentThread().isInterrupted()) {

                try {
                    System.out.println("线程阻塞:"+Thread.currentThread().getName());
                    ItemVo<Order> take = delayQueue.take();
                    System.out.println("线程拿到数据:" + Thread.currentThread().getName());
                    if (take != null) {
                        Order order = take.getData();
                        //检查数据库订单状态
                        ConcurrentHashMap db = OrderController.db;
                        Order o = (Order) db.get(order.getId());
                        if (o.getStatus() == Order.OrderStatus.UNPAY) {
                            //更新订单
                            System.out.println("更新订单:" + order.getName());
                            order.setStatus(Order.OrderStatus.EXPIRED);
                            db.put(order.getId(), order);
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private Thread takeOrder;

    @PostConstruct
    public void init() {
        System.out.println("启动延时队列处理服务");
        takeOrder = new Thread(new TakeOrder());
        takeOrder.start();
    }

    @PreDestroy
    public void close() {
        System.out.println("启动延时队列处理服务");
        takeOrder.interrupt();
    }
}

package com.matosiki;

import com.matosiki.audit.AuditOperationRecordAware;
import com.matosiki.listener.AuditOperationEventListener;
import com.matosiki.listener.AuditOperationRecordAwareImpl;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
public class SpringBootDataJpaAuditApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDataJpaAuditApplication.class, args);
    }


    @Autowired
    private AuditOperationEventListener auditOperationEventListener;

    @Resource
    private HibernateEntityManagerFactory hemf;

    @Bean
    AuditOperationRecordAware auditOperationRecordAware() {
        return new AuditOperationRecordAwareImpl();
    }


    @PostConstruct
    public void registerListeners() {
        EventListenerRegistry registry = ((SessionFactoryImpl) hemf.getSessionFactory()).getServiceRegistry().getService(
                EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(auditOperationEventListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(auditOperationEventListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(auditOperationEventListener);
    }

}

package com.matosiki.email;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
    private Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private EmailSender emailSender;
    @Test
    public void contextLoads() {
        //String to = "1339817509@qq.com";
        String to = "matosiki@163.com";
        String subject = "动力系滴";
        String content = "曾经沧海难为水，除却巫山不是云，取次花丛懒回顾，半月修道半缘君。";
        boolean result = emailSender.sender(to, subject, content);
        logger.info("-------------======---------------" + result);
    }

}

package com.matosiki.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.matosiki.api.CityDubboService;
import com.matosiki.domain.City;

// 注册为 Dubbo 服务
@Service(version = "1.0.0")
public class CityServiceImpl implements CityDubboService {

    public City findCityByName(String cityName) {
        return new City(1L, 2L, "杭州", "实现我梦想的地方，也是我逃离的地方。");
    }
}
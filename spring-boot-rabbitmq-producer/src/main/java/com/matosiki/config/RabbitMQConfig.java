package com.matosiki.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    /**
     * 简单 direct 模式
     */
    @Bean
    public Queue queue() {
        return new Queue("queue");
    }


    /**
     * topic 模式
     */
    @Bean(name = "message")
    public Queue queueMessage() {
        return new Queue("topic.message");
    }

    /**
     * topic 模式
     */
    @Bean(name = "messages")
    public Queue queueMessages() {
        return new Queue("topic.messages");
    }

    /**
     * 添加Topic交换机
     */
    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("exchange");
    }

    /**
     * topic 模式  添加到交换机上
     */
    @Bean
    Binding bindingExchangeMessage(@Qualifier("message") Queue queueMessage, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessage).to(exchange).with("topic.message");
    }

    /**
     * topic 模式  添加到交换机上
     */
    @Bean
    Binding bindingExchangeMessages(@Qualifier("messages") Queue queueMessages, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessages).to(exchange).with("topic.#");//*表示一个词,#表示零个或多个词
    }

    /**
     * fanout 广播模式
     */
    @Bean(name = "Amessage")
    public Queue AMessage() {
        return new Queue("fanout.A");
    }

    /**
     * fanout 广播模式
     */
    @Bean(name = "Bmessage")
    public Queue BMessage() {
        return new Queue("fanout.B");
    }

    /**
     * 添加fanout交换机
     */
    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanoutExchange");//配置广播路由器
    }

    /**
     * 将消息绑定到fanout交换机上
     *
     * @param AMessage
     *         消息A
     * @param fanoutExchange
     * @return
     */
    @Bean
    Binding bindingExchangeA(@Qualifier("Amessage") Queue AMessage, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(AMessage).to(fanoutExchange);
    }

    /**
     * 将消息绑定到fanout交换机上
     *
     * @param BMessage
     *         消息B
     * @param fanoutExchange
     * @return
     */
    @Bean
    Binding bindingExchangeB(@Qualifier("Bmessage") Queue BMessage, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(BMessage).to(fanoutExchange);
    }


}
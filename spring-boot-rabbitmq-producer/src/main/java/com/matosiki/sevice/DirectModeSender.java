package com.matosiki.sevice;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Direct模式发送
 */
@Component
public class DirectModeSender {

    @Autowired
    private AmqpTemplate template;

    public void send() {

        template.convertAndSend("queue", "hello,rabbit~");
    }
}
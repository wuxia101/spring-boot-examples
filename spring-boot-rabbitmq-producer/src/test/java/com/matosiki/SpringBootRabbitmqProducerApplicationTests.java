package com.matosiki;

import com.matosiki.sevice.DirectModeSender;
import com.matosiki.sevice.FanoutModeSender;
import com.matosiki.sevice.TopicModeSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRabbitmqProducerApplicationTests {

    @Test
    public void contextLoads() {
        System.out.println("===========================");
        System.out.println("===========================");
        System.out.println("===========启动成功=========");
        System.out.println("===========================");
        System.out.println("===========================");
        System.out.println("===========================");
    }

    @Autowired
    private DirectModeSender directModeSender;

    @Autowired
    private TopicModeSender topicModeSender;
    @Autowired
    private FanoutModeSender fanoutModeSender;

    @Test
    public void testDirectModeSender() {
        directModeSender.send();
    }

    @Test
    public void testTopicModeSender() {
        topicModeSender.send();
    }

    @Test
    public void testFanoutModeSender() {
        fanoutModeSender.send();
    }
}

package com.matosiki;

import com.rabbitmq.client.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
@RunWith(JUnit4.class)
public class RabbitMQBasicTest {


    @Test
    public void testConnect() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("172.24.232.210");
        factory.setUsername("root");
        factory.setPassword("root");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
    }

    @Test
    public void testProducer() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("172.24.232.210");
        factory.setUsername("root");
        factory.setPassword("root");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        /*=======================================*/
        channel.queueDeclare("products_queue", false, false, false, null);
        String message = "product details";
        channel.basicPublish("", "products_queue", null, message.getBytes());
        channel.close();
        connection.close();
    }

    @Test
    public void testConsumer() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("172.24.232.210");
        factory.setUsername("root");
        factory.setPassword("root");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        /*=========================================*/
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(
                    String consumerTag,
                    Envelope envelope,
                    AMQP.BasicProperties properties,
                    byte[] body) throws IOException {

                String message = new String(body, "UTF-8");
                // process the message
                System.out.println("打印获取到的消息：");
                System.out.println("==================>"+ message);
            }
        };
        channel.basicConsume("products_queue", true, consumer);
    }
}
package com.matosiki.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * direct 模式接受
 */
@Component
public class DirectModeReceive {

    @RabbitListener(queues = "queue")    //监听器监听指定的Queue
    public void processC(String str) {
        System.out.println("Receive:" + str);
    }

}